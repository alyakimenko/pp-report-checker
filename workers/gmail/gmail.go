package gmail

import (
	"crypto/tls"
	"database/sql"
	"io"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/alyakimenko/pp-report-checker/pkg/utils"

	"gitlab.com/alyakimenko/pp-report-checker/config"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	"gitlab.com/alyakimenko/pp-report-checker/pkg/models"
)

const timeout = time.Minute * 2

var (
	// Whitespace here is 160 char code , not standard 32
	amountRegex   = regexp.MustCompile("[0-9]* *[0-9]+,[0-9]+")
	currencyRegex = regexp.MustCompile("RUB|USD")
)

func StartComparingWithGmail(db *sql.DB, cfg *config.Config) {
	var lastCheckDate = time.Date(2000, time.August, 23, 12, 23, 65, 2, time.Local)

	for {
		payments, err := fetchFromGmail(lastCheckDate, cfg)
		if err != nil {
			log.Println(err)
		}

		log.Printf("Found %d new payments at Gmail\n", len(payments))

		if err := checkPayments(db, cfg, payments); err != nil {
			log.Println(err)
		}

		lastCheckDate = time.Now().Add(time.Duration(-1) * time.Minute)
		time.Sleep(timeout)
	}
}

func checkPayments(db *sql.DB, cfg *config.Config, incoming Payments) error {
	pending, err := models.Payment{}.GetAllByStatus(db, models.StatusPending)
	if err != nil {
		return err
	}

	if len(incoming) == 0 {
		for _, pend := range pending {
			if err := pend.SetLastCheckDate(db, time.Now()); err != nil {
				return err
			}
		}
	}

	for _, pend := range pending {
		if time.Now().Sub(pend.CreatedAt) >= (time.Minute * 15) {
			if err := pend.SetStatus(db, models.StatusExpired); err != nil {
				return err
			}
			continue
		}

		for i, inc := range incoming {
			if pend.Amount == inc.Amount {
				if inInterval(pend.CreatedAt, pend.CreatedAt.Add(time.Minute*15), inc.Date) {
					if pend.Currency == inc.Currency {
						if err := pend.SetStatus(db, models.StatusSuccess); err != nil {
							log.Println(err)
						}

						callback := &utils.PaymentCallback{
							Email:         pend.Email,
							Currency:      pend.Currency,
							Amount:        pend.Amount,
							AmountWithFee: inc.AmountWithFee,
							PaymentID:     pend.ExternalPaymentID,
							Status:        models.StatusSuccess,
						}
						callback.PaymentHash = callback.Hash(cfg.APIKey)

						if err := callback.Send(cfg.CallbackURL); err != nil {
							return err
						}
						log.Printf("Send callback on %s\n", cfg.CallbackURL)
						log.Printf("Email: %s, Currency: %s, Amount: %.2f, AmountWithFee: %.2f\n",
							callback.Email, callback.Currency, callback.Amount, callback.AmountWithFee)
						log.Printf("Hash: %s\n", callback.PaymentHash)

						incoming = append(incoming[:i], incoming[i+1:]...)
					}
				}
			} else {
				if err := pend.SetLastCheckDate(db, time.Now()); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func inInterval(start time.Time, end time.Time, t time.Time) bool {
	return t.After(start) && t.Before(end) || t.Equal(start)
}

func fetchFromGmail(lastCheckDate time.Time, cfg *config.Config) (Payments, error) {
	var payments Payments

	// Connect to server
	c, err := client.DialTLS("imap.gmail.com:993", &tls.Config{InsecureSkipVerify: true})
	if err != nil {
		log.Fatal(err)
	}

	defer c.Logout()

	// Login
	if err := c.Login(cfg.Email, cfg.Pass); err != nil {
		log.Fatal(err)
	}

	// Select INBOX
	mbox, err := c.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}

	// Get the last message
	if mbox.Messages == 0 {
		log.Fatal("No message in mailbox")
	}
	seqSet := new(imap.SeqSet)
	seqSet.AddRange(uint32(1), mbox.Messages)

	// Get the whole message body
	var section imap.BodySectionName
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 5)
	go func() {
		if err := c.Fetch(seqSet, items, messages); err != nil {
			log.Fatal(err)
		}
	}()

	for msg := range messages {
		if msg == nil {
			log.Fatal("Server didn't returned message")
		}

		r := msg.GetBody(&section)
		if r == nil {
			log.Fatal("Server didn't returned message body")
		}

		// Create a new mail reader
		mr, err := mail.CreateReader(r)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		// Print some info about the message
		header := mr.Header
		if from, err := header.AddressList("From"); err == nil {
			if from[0].Address != "service@paypal.com" {
				continue
			}
		}
		if subject, err := header.Subject(); err == nil {
			if subject != "Вы получили платеж" {
				continue
			}
		}

		date, err := header.Date()
		if err != nil {
			continue
		}

		if date.Before(lastCheckDate) {
			continue
		}

		newPayment := Payment{
			Date: date,
		}
		// Process each message's part
		for {
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Fatal(err)
			}

			switch p.Header.(type) {
			case *mail.InlineHeader:
				b, _ := ioutil.ReadAll(p.Body)

				fullIndex := strings.Index(string(b), "Полученная сумма")
				lFull := len("Полученная сумма")

				fullAmount := amountRegex.FindString(string(b)[fullIndex+lFull:])
				fullAmount = strings.ReplaceAll(fullAmount, string(160), "")
				fullAmount = strings.ReplaceAll(fullAmount, ",", ".")

				currency := currencyRegex.FindString(string(b)[fullIndex+lFull:])

				newPayment.Currency = currency
				newPayment.Amount, err = strconv.ParseFloat(fullAmount, 64)
				if err != nil {
					continue
				}

				amountWithFee := "no fee"
				var lFee int
				feeIndex := strings.Index(string(b), "Итого")
				if feeIndex != -1 {
					lFee = len("Итого")
					amountWithFee = amountRegex.FindString(string(b)[feeIndex+lFee:])
					amountWithFee = strings.ReplaceAll(amountWithFee, string(160), "")
					amountWithFee = strings.ReplaceAll(amountWithFee, ",", ".")
					newPayment.AmountWithFee, err = strconv.ParseFloat(amountWithFee, 64)
					if err != nil {
						continue
					}
				} else {
					newPayment.AmountWithFee, err = strconv.ParseFloat(fullAmount, 64)
					if err != nil {
						continue
					}
				}

				log.Printf("Found payment! Amount: %s, AmountWithFee: %s, Currency: %s",
					fullAmount, amountWithFee, currency)
			}
		}
		payments = append(payments, newPayment)
	}

	return payments, nil
}
