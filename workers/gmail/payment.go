package gmail

import "time"

type Payments []Payment

type Payment struct {
	Date          time.Time
	Amount        float64
	AmountWithFee float64
	Currency      string
}
