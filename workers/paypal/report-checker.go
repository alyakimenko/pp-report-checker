package paypal

import (
	"database/sql"
	"encoding/csv"
	"io"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unicode/utf16"

	"gitlab.com/alyakimenko/pp-report-checker/pkg/models"
)

const timeout = time.Second * 30

type Index int

const (
	DateIndex         Index = 0
	TimeIndex         Index = 1
	CurrencyIndex     Index = 6
	BruttoAmountIndex Index = 7
	FeeIndex          Index = 8
	NettoAmountIndex  Index = 9
	FromEmailIndex    Index = 10
	TypeIndex         Index = 40
)

func StartPayPalReportChecker(db *sql.DB) {
	for {
		startPayPalReportChecker(db)
		time.Sleep(timeout)
	}
}

func startPayPalReportChecker(db *sql.DB) {
	report, err := os.Open("report.csv")
	if err != nil {
		panic(err)
	}
	defer report.Close()

	reader := csv.NewReader(report)
	reader.LazyQuotes = true

	payments, err := models.Payment{}.GetAllPending(db)
	if err != nil {
		log.Println(err)
	}

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		payType := utf16.Encode([]rune(record[TypeIndex]))

		if !reflect.DeepEqual(payType, utf16.Encode([]rune("Кредит"))) {
			continue
		}

		var temp []models.Payment

		bruttoAmount, err := parseFloatString(record[BruttoAmountIndex])
		if err != nil {
			log.Println(err)
			continue
		}

		for _, payment := range payments {
			if payment.Email == record[FromEmailIndex] && payment.Status != models.StatusSuccess && int(payment.Amount) == int(bruttoAmount) {
				payment.Status = models.StatusSuccess
				temp = append(temp, payment)
				// TODO: Send callback
				// SendCallback(url, body)...
			}
			if err := payment.SetLastCheckDate(db, time.Now()); err != nil {
				log.Println(err)
				continue
			}
		}

		for _, t := range temp {
			if err := t.SetStatus(db, models.StatusSuccess); err != nil {
				log.Println(err)
			}
		}
	}
}

func parseFloatString(in string) (float64, error) {
	in = strings.ReplaceAll(in, ",", ".")
	in = strings.ReplaceAll(in, "\u00a0", "")
	in = strings.ReplaceAll(in, " ", "")

	out, err := strconv.ParseFloat(in, 32)
	if err != nil {
		return 0, err
	}
	return out, nil
}
