.PHONY: build
build:  ## build the API server binary
	go build -o pp-report-checker main.go
