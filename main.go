package main

import (
	"database/sql"
	"html/template"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/alyakimenko/pp-report-checker/app"
	"gitlab.com/alyakimenko/pp-report-checker/config"
	"gitlab.com/alyakimenko/pp-report-checker/controllers"
	"gitlab.com/alyakimenko/pp-report-checker/workers/gmail"
)

func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	db, err := sql.Open("sqlite3", cfg.DB)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	go gmail.StartComparingWithGmail(db, cfg)

	router := buildRouter(db, cfg)
	if err = router.Start(":6969"); err != nil {
		panic(err)
	}
}

func buildRouter(db *sql.DB, cfg *config.Config) *echo.Echo {
	e := echo.New()
	e.Static("/swagger", "swaggerui")

	renderer := &app.TemplateRenderer{
		Templates: template.Must(template.ParseGlob("templates/*.html")),
	}
	e.Renderer = renderer

	e.Debug = true
	e.HideBanner = true

	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())

	controllers.PaymentsController{}.Init(db, cfg.APIKey, e.Group("/payments"))

	return e
}
