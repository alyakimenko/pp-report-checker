package controllers

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/alyakimenko/pp-report-checker/pkg/models"
)

type PaymentsController struct {
	db *sql.DB
}

func (pc PaymentsController) Init(db *sql.DB, apiKey string, g *echo.Group) {
	pc.db = db

	g.Use(middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		return key == apiKey, nil
	}))
	g.POST("", pc.CreateNewPayment)
	g.GET("", pc.GetAll)
	g.GET("/pending", pc.GetAllPending)
	g.GET("/expired", pc.GetAllExpired)
	g.GET("/html", pc.RenderPayments)
}

func (pc PaymentsController) CreateNewPayment(c echo.Context) error {
	var (
		payment models.Payment
	)

	var reqBody = struct {
		Email             string  `json:"email"`
		Amount            float64 `json:"amount"`
		Currency          string  `json:"currency"`
		ExternalPaymentID int     `json:"payment_id"`
	}{}
	if err := c.Bind(&reqBody); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	reqBody.Currency = strings.ToUpper(reqBody.Currency)

	payment = models.Payment{
		Email:             reqBody.Email,
		Amount:            reqBody.Amount,
		Currency:          reqBody.Currency,
		System:            models.PayPal,
		Status:            models.StatusPending,
		ExternalPaymentID: reqBody.ExternalPaymentID,
	}

	if err := payment.Validate(); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err := payment.Save(pc.db); err != nil {
		return c.JSON(http.StatusInternalServerError, "Error on saving")
	}

	log.Println("Created new payment:", payment)
	return c.JSON(http.StatusOK, struct {
		RedirectURL string `json:"redirect_url"`
	}{
		RedirectURL: fmt.Sprintf("https://www.paypal.me/astroproxy/%.2f%s", payment.Amount, payment.Currency),
	})
}

func (pc PaymentsController) RenderPayments(c echo.Context) error {
	if c.Request().Method != http.MethodGet {
		return c.JSON(http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed))
	}

	payments, err := models.Payment{}.GetAll(pc.db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	type response struct {
		Payments []models.Payment
	}

	res := &response{
		Payments: payments,
	}

	return c.Render(http.StatusOK, "payments.html", res)
}

func (pc PaymentsController) GetAll(c echo.Context) error {
	payments, err := models.Payment{}.GetAll(pc.db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	type response struct {
		Payments []models.Payment `json:"payments"`
	}

	res := &response{
		Payments: payments,
	}

	return c.JSON(http.StatusOK, res)
}

func (pc PaymentsController) GetAllPending(c echo.Context) error {
	payments, err := models.Payment{}.GetAllByStatus(pc.db, models.StatusPending)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	type response struct {
		Payments []models.Payment `json:"payments"`
	}

	res := &response{
		Payments: payments,
	}

	return c.JSON(http.StatusOK, res)
}

func (pc PaymentsController) GetAllExpired(c echo.Context) error {
	payments, err := models.Payment{}.GetAllByStatus(pc.db, models.StatusExpired)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	type response struct {
		Payments []models.Payment `json:"payments"`
	}

	res := &response{
		Payments: payments,
	}

	return c.JSON(http.StatusOK, res)
}
