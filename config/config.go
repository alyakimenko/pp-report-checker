package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Config struct {
	Email       string `json:"email"`
	Pass        string `json:"pass"`
	DB          string `json:"db"`
	CallbackURL string `json:"callback_url"`
	APIKey      string `json:"api_key"`
}

func LoadConfig() (*Config, error) {
	configFile, err := os.Open("./config/config.json")
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(configFile)
	if err != nil {
		return nil, err
	}

	var config Config
	if err := json.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	return &config, nil
}
