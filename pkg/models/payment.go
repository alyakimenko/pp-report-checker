package models

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"regexp"
	"time"
)

var (
	re = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

type PaymentSystem string

const (
	PayPal PaymentSystem = "PayPal"
)

type PaymentStatus string

const (
	StatusPending PaymentStatus = "Pending"
	StatusSuccess PaymentStatus = "Success"
	StatusExpired PaymentStatus = "Expired"
)

type Payment struct {
	ID                int           `json:"id"`
	Email             string        `json:"email"`
	Amount            float64       `json:"amount"`
	Currency          string        `json:"currency"`
	CreatedAt         time.Time     `json:"created_at"`
	LastCheckDate     time.Time     `json:"last_check_date"`
	System            PaymentSystem `json:"payment_system"`
	Status            PaymentStatus `json:"status"`
	ExternalPaymentID int           `json:"payment_id"`
}

func (p Payment) String() string {
	return fmt.Sprintf("email: %s, amount: %.2f, curreny: %s, system: %s, status: %s, created_at: %s",
		p.Email, p.Amount, p.Currency, p.System, p.Status, p.CreatedAt)
}

func (p Payment) Validate() error {
	if p.Currency != "RUB" && p.Currency != "USD" {
		return errors.New("wrong currency")
	}
	if !re.MatchString(p.Email) {
		return errors.New("incorrect email")
	}
	return nil
}

func (p Payment) FormattedCreatedAt() string {
	return p.CreatedAt.Format(time.RFC822)
}

func (p Payment) FormattedLastCheckDate() string {
	return p.LastCheckDate.Format(time.RFC822)
}

func (p Payment) Save(db *sql.DB) error {
	_, err := db.Exec(
		`insert into payments (email, amount, currency, created_at, last_check_date, payment_system, status, external_payment_id
                      ) values ($1, $2, $3, $4, $5, $6, $7, $8)`, p.Email, p.Amount, p.Currency, time.Now(), time.Now(), p.System, StatusPending, p.ExternalPaymentID)
	return err
}

func (p Payment) SetStatus(db *sql.DB, status PaymentStatus) error {
	_, err := db.Exec("update payments set status = $1 where id = $2", status, p.ID)
	return err
}

func (p Payment) SetLastCheckDate(db *sql.DB, date time.Time) error {
	_, err := db.Exec("update payments set last_check_date = $1 where id = $2", date, p.ID)
	return err
}

func (p Payment) GetAll(db *sql.DB) ([]Payment, error) {
	rows, err := db.Query("select * from payments order by created_at desc;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var payments []Payment

	for rows.Next() {
		p := Payment{}
		err := rows.Scan(&p.ID, &p.Email, &p.Amount, &p.Currency, &p.CreatedAt, &p.LastCheckDate, &p.System, &p.Status, &p.ExternalPaymentID)
		if err != nil {
			log.Println(err)
			continue
		}
		payments = append(payments, p)
	}

	return payments, nil
}

func (p Payment) GetAllByStatus(db *sql.DB, status PaymentStatus) ([]Payment, error) {
	rows, err := db.Query("select * from payments where status = ?", status)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var payments []Payment

	for rows.Next() {
		p := Payment{}
		err := rows.Scan(&p.ID, &p.Email, &p.Amount, &p.Currency, &p.CreatedAt, &p.LastCheckDate, &p.System, &p.Status, &p.ExternalPaymentID)
		if err != nil {
			log.Println(err)
			continue
		}
		payments = append(payments, p)
	}

	return payments, nil
}
