package utils

import (
	"reflect"
	"sync"

	"gitlab.com/alyakimenko/pp-report-checker/pkg/models"
)

// ConcurrentSlice type that can be safely shared between goroutines
type ConcurrentSlice struct {
	sync.RWMutex
	Items []models.Payment `json:"items"`
}

// ConcurrentSliceItem contains the index/value pair of an item in a
// concurrent slice
type ConcurrentSliceItem struct {
	Index int
	Value models.Payment
}

// NewConcurrentSlice creates a new concurrent slice
func NewConcurrentSlice() *ConcurrentSlice {
	cs := &ConcurrentSlice{
		Items: make([]models.Payment, 0),
	}

	return cs
}

func (cs *ConcurrentSlice) Update(index int, item models.Payment) {
	cs.Lock()
	cs.Unlock()

	cs.Items[index] = item
}

// Append adds an item to the concurrent slice
func (cs *ConcurrentSlice) Append(item models.Payment) {
	cs.Lock()
	defer cs.Unlock()

	cs.Items = append(cs.Items, item)
}

// Delete deletes an item from the concurrent slice
func (cs *ConcurrentSlice) Delete(item models.Payment) {
	cs.Lock()
	defer cs.Unlock()

	if len(cs.Items) == 0 {
		return
	}

	for i := range cs.Items {
		if reflect.DeepEqual(cs.Items[i], item) {
			cs.Items = cs.Items[:i+copy(cs.Items[i:], cs.Items[i+1:])]
		}
	}
}

// Iter iterates over the items in the concurrent slice
func (cs *ConcurrentSlice) Iter() <-chan ConcurrentSliceItem {
	c := make(chan ConcurrentSliceItem)

	f := func() {
		cs.Lock()
		defer cs.Unlock()
		for index, value := range cs.Items {
			c <- ConcurrentSliceItem{index, value}
		}
		close(c)
	}
	go f()

	return c
}

// Len returns current len of the concurrent slice
func (cs *ConcurrentSlice) Len() int {
	cs.Lock()
	defer cs.Unlock()

	return len(cs.Items)
}
