package utils

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/alyakimenko/pp-report-checker/pkg/models"
)

type PaymentCallback struct {
	Email         string               `json:"email"`
	Currency      string               `json:"currency"`
	Amount        float64              `json:"amount"`
	AmountWithFee float64              `json:"amount_with_fee"`
	PaymentID     int                  `json:"payment_id"`
	Status        models.PaymentStatus `json:"status"`
	PaymentHash   string               `json:"signature"`
}

func (pc *PaymentCallback) Hash(secretKey string) string {
	toHash := strings.Join([]string{
		strconv.Itoa(pc.PaymentID),
		pc.Email,
		fmt.Sprintf("%.2f", pc.Amount),
		fmt.Sprintf("%.2f", pc.AmountWithFee),
		pc.Currency,
		string(pc.Status),
		secretKey,
	}, ":")

	md5Hash := md5.Sum([]byte(toHash))
	return base64.StdEncoding.EncodeToString(md5Hash[:])
}

func (pc *PaymentCallback) Send(url string) (err error) {
	data, err := json.Marshal(pc)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(data)
	_, err = http.Post(url, "application/json", buf)

	return
}
