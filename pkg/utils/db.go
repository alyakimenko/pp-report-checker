package utils

import (
	"encoding/json"
	"os"
)

func SaveToDB(payments *ConcurrentSlice) error {
	file, err := os.Create("data.json")
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := json.Marshal(payments)
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}
	return nil
}
