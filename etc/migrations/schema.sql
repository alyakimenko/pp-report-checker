create table if not exists payments
(
    id                integer       primary key autoincrement unique,
    email             text          not null,
    amount            real          not null,
    currency          text          not null,
    created_at        datetime      not null,
    last_check_date   datetime,
    payment_system    text          not null,
    status            text          not null
);

alter table payments
add column external_payment_id integer default 0;